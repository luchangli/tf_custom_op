import ctypes
import time
import tensorflow as tf
import numpy as np

from tensorflow.python.framework import load_library

full_lib_path = "build/libcustom_op.so"
zero_out_module = tf.load_op_library(full_lib_path)

zero_out = zero_out_module.zero_out

data = np.ones((2,2), dtype=np.float32)

data1_tf = zero_out(data)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for i in range(2):
        data1 = sess.run(data1_tf)
        print(data)
        print(data1)
