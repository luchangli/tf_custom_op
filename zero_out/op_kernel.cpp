#define EIGEN_USE_GPU

#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/util/gpu_kernel_helper.h"
#include "op_kernel.h"
#include <iostream>

using namespace tensorflow;
using namespace std;

using GPUDevice = Eigen::GpuDevice;

class ZeroOutOp : public OpKernel {
 public:
  string deviceTypeStr = "";

  explicit ZeroOutOp(OpKernelConstruction* context) : OpKernel(context) {
    deviceTypeStr = context->device_type().type_string();
    cout << "device type str: " << deviceTypeStr << endl;
  }

  void Compute(OpKernelContext* context) override {
    // Grab the input tensor
    const Tensor& input_tensor = context->input(0);

    // Create an output tensor
    Tensor* output_tensor = NULL;
    OP_REQUIRES_OK(context, context->allocate_output(0, input_tensor.shape(), &output_tensor));

    void* inData = (void*)input_tensor.tensor_data().data();
    void* outData = (void*)output_tensor->tensor_data().data();
  }
};


REGISTER_KERNEL_BUILDER(Name("ZeroOut").Device(DEVICE_GPU), ZeroOutOp);

