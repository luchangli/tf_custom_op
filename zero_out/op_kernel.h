
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


void exp_cuda(const float* d_in, float* d_out, unsigned int data_num, cudaStream_t cstream);
