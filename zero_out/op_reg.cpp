#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"

using namespace std;

using namespace tensorflow;

REGISTER_OP("ZeroOut")
    .Input("to_zero: float")
    .Output("zeroed: float");

