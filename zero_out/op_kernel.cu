#include "op_kernel.h"
#include <iostream>
// using namespace std;

__global__ void exp_kernel(const float* d_in, float* d_out, unsigned int data_num) {

  unsigned int gid = threadIdx.x + blockIdx.x * blockDim.x;

  if (gid < data_num) {
    d_out[gid] = __expf(d_in[gid]);
  }

}

#define ThreadsPerBlock 32

void exp_cuda(const float* d_in, float* d_out, unsigned int data_num, cudaStream_t cstream) {

  int BlockDim = ThreadsPerBlock;
  int BlockNum = ((data_num + ThreadsPerBlock - 1) / ThreadsPerBlock);

  exp_kernel << <BlockNum, BlockDim, 0, cstream >> >(d_in, d_out, data_num);
}

